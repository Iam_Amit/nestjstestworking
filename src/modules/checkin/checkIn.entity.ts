import {
  BaseEntity, CreateDateColumn,
  Entity,
  JoinColumn, ManyToOne,
  OneToOne, PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm';
import { TimePunch } from '../timePunch/timePunch.entity';
import { UserInfo } from '../userInfo/userInfo.entity';

@Entity()
export class CheckIn extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(()=> UserInfo,  )
  userId: UserInfo;

  @OneToOne(() => TimePunch, { nullable: true })
  @JoinColumn()
  checkIn: TimePunch;

  @OneToOne(() => TimePunch, { nullable: true })
  @JoinColumn()
  checkOut: TimePunch;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
