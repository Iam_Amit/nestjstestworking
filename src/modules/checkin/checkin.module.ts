import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TimePunchRepository } from '../timePunch/timePunch.repository';
import { UserInfoRepository } from '../userInfo/userInfo.repository';
import { CheckInController } from './checkin.controller';
import { CheckInRepository } from './checkIn.repository';
import { CheckInService } from './checkin.service';

@Module({
  imports: [TypeOrmModule.forFeature([CheckInRepository, TimePunchRepository, UserInfoRepository])],
  controllers: [CheckInController],
  providers: [CheckInService],
})
export class CheckInModule {}
