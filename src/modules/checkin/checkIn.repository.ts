import { CheckInDto } from 'src/dto/checkIn/checkIn.dto';
import { EntityRepository, Repository } from 'typeorm';
import { TimePunch } from '../timePunch/timePunch.entity';
import { UserInfo } from '../userInfo/userInfo.entity';
import { CheckIn } from './checkIn.entity';
// import { CheckInService } from "./checkin.service";

@EntityRepository(CheckIn)
export class CheckInRepository extends Repository<CheckIn> {

  async addNewCheckIn(newTimePunch: TimePunch,user:UserInfo) {
    let newCheckIn = new CheckIn();
    newCheckIn.checkIn = newTimePunch;
    newCheckIn.userId = user;
    console.log('UserId => ', newTimePunch)
    return await newCheckIn.save();
  }

  async updateCheckOut(updateTimePunch: TimePunch,user:UserInfo) {
    let newCheckOut = new CheckIn();
    newCheckOut.checkOut = updateTimePunch;
    newCheckOut.userId = user;
    console.log('UserId => ', updateTimePunch)
    return await newCheckOut.save();
  }
}
