import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Param,
  Post,
  Put,
  Res,
} from '@nestjs/common';
import { CheckInDto } from 'src/dto/checkIn/checkIn.dto';
import { CheckInService } from './checkin.service';
import { Response } from 'express';
import { TimePunchDto } from 'src/dto/timePunch/timePunch.dto';
import { CheckInUpdateDto } from 'src/dto/checkIn/checkInUpdate.dto';
// import { UserInfoService } from '../userInfo/userinfo.service';

@Controller('checkin')
export class CheckInController {
  constructor(private checkinService: CheckInService,
    // private userInfoServices: UserInfoService
    ) {}

  // Adding the data

  @Post('PunchTime')
  async addTimePunchData(@Body() body: TimePunchDto, @Res() res: Response) {
    const val = await this.checkinService.createPunchTime(body);
    console.log('CheckIn controller => ',body.userId)
    res.status(HttpStatus.OK).send(val);
  }

  // Updating the data in NestJs
  @Put('checkOut')
  async updateCheckInData(
    
    @Body() body: TimePunchDto,
    @Res() res: Response,
  ) {
    const updateVal = await this.checkinService.checkInUpdate(body);
    res.status(HttpStatus.OK).send(updateVal)
  }


  // @Get(':id')
  // async readUserInfoId(@Param() id: number, @Res() res: Response) {
  //   const val = await this.userInfoServices.getUserInfoById(id);
  //   res.status(HttpStatus.OK).send(val);
  // }
  
}
