import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { userInfo } from 'os';
import { TimePunchDto } from 'src/dto/timePunch/timePunch.dto';
import { UserSignupDto } from 'src/dto/userSignup.dto';
import { TimePunchRepository } from '../timePunch/timePunch.repository';
import { UserInfo } from '../userInfo/userInfo.entity';
import { UserInfoModule } from '../userInfo/userinfo.module';
import { UserInfoRepository } from '../userInfo/userInfo.repository';
import { CheckIn } from './checkIn.entity';
import { CheckInRepository } from './checkIn.repository';

@Injectable()
export class CheckInService {
  constructor(
    @InjectRepository(CheckInRepository)
    private checkInRepo: CheckInRepository,
    @InjectRepository(TimePunchRepository)
    private timePunchRepo: TimePunchRepository,
    @InjectRepository(UserInfoRepository)
    private userInfoRepo: UserInfoRepository,
  ) {}

  // Creating the PunchIn (Checkin)
  async createPunchTime(body: TimePunchDto): Promise<CheckIn> {
    let newTimePunch = await this.timePunchRepo.addTimePunchData(body);
    // let userInfoData = await this.userInfoRepo.find({username: userInfo.username})
    let user = await this.userInfoRepo.findOne({ id: body.userId });
    // console.log('UserInfo Object = >', userInfoRepo)

    console.log('UserId => ', body.userId);
    return this.checkInRepo.addNewCheckIn(newTimePunch,user);
    // pass the userInfoObject
  }

  // Updating the data checkOut
  async checkInUpdate(body: TimePunchDto): Promise<CheckIn> {
    
      let user = await this.userInfoRepo.findOne({ id: body.userId });
       console.log('user =>',user)
      // if(!user) return 
      
      let checkOutData = await this.checkInRepo.find({where: {userId: body.userId, checkOut: null}})

      let finalData = checkOutData.sort()
      let updateTimePunch = await this.timePunchRepo.addTimePunchData(body);
      
      finalData[0].checkOut = updateTimePunch;
      return finalData[0].save()
  }
}
