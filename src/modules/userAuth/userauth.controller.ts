import {
  Controller,
  Post,
  Body,
  Res,
  HttpStatus,
  Get,
  Put,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { UserAuthServices } from './userauth.service';
import { Response } from 'express';
import { UserSignupDto } from '../../dto/userSignup.dto';
import { UserLoginDto } from 'src/dto/userLogin.dto';
// import { LocalAuthGuard } from 'src/auth/local.auth.guard';

@Controller('userAuth')
export class UserAuthController {
  constructor(private userAuthService: UserAuthServices) {}

  // Add the Data
  // @UseGuards(LocalAuthGuard)
  @Post()
  async addUserInfo(@Body() body: UserSignupDto, @Res() res: Response) {
    const val = await this.userAuthService.createUser(body);
    const { id, email, password, age } = val;
    res.status(HttpStatus.OK).send({
      id,
      email,
      password,
      age,
    });
  }

  // Login User API

  @Post()
  async login(@Body() body: UserLoginDto, @Res() res: Response) {
    const loginVal: any = await this.userAuthService.userLogin(body);
    // const { email, password } = loginVal;
    res.status(HttpStatus.OK).send({
      msg: ' User Logged in Successfully',
    });
  }
}
