/*
https://docs.nestjs.com/modules
*/

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserAuthController } from './userauth.controller';
import { UserAuthRepository } from './userAuth.repository';
import { UserAuthServices } from './userauth.service';

@Module({
  imports: [TypeOrmModule.forFeature([UserAuthRepository])],
  controllers: [UserAuthController],
  providers: [UserAuthServices],
})
export class UserAuthModule {}
