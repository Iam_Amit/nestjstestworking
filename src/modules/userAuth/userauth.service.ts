import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserAuthRepository } from './userAuth.repository';
import { UserSignupDto } from 'src/dto/userSignup.dto';
import { UserAuth } from './userAuth.entity';
import { CreateUserDto } from 'src/dto/createUser.dto';
// import { AuthService } from 'src/auth/services/auth/auth.service';
import { UpdateUserDto } from 'src/dto/updateUser.dto';
import { UserLoginDto } from 'src/dto/userLogin.dto';

@Injectable()
export class UserAuthServices {
  constructor(
    @InjectRepository(UserAuthRepository)
    private userAuthRepo: UserAuthRepository,
  ) {}

  // Adding the data
  createUser(userAuth: UserSignupDto): Promise<UserAuth> {
    return this.userAuthRepo.addUserAuth(userAuth);
  }

  // Login User Data
  async userLogin(data: UserLoginDto) {
    const emailExist = await this.userAuthRepo.findOne({ email: data.email });

    if (!emailExist) throw new Error('Email not exist ');

    console.log('qwertrwqer =>', emailExist);
    const verifyPasswords = await this.userAuthRepo.comparePasswords(
      data.password,
      emailExist.password,
    );

    if (!verifyPasswords) throw new Error(' Password do not match !');

    return 'User loged in Successfuly!';
  }
}
