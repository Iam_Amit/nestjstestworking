import { Repository, EntityRepository } from 'typeorm';
import { UserAuth } from './userAuth.entity';
import { UserSignupDto } from 'src/dto/userSignup.dto';
// import { from, Observable } from 'rxjs';
import { hash, compare } from 'bcrypt';
import { InternalServerErrorException } from '@nestjs/common';
// import { JwtService } from '@nestjs/jwt';

@EntityRepository(UserAuth)
export class UserAuthRepository extends Repository<UserAuth> {
  async addUserAuth(userAuth: UserSignupDto): Promise<UserAuth> {
    const { email, password, username } = userAuth;
    const newUser = new UserAuth();
    newUser.username = username;

    try {
      newUser.password = await this.hashPassword(password);
    } catch (e) {
      throw new InternalServerErrorException({ error: e });
    }
    // newUser.age = age;
    newUser.email = email;
    newUser.isActive = true;
    await newUser.save();
    return newUser;
  }
  async hashPassword(password: string): Promise<string> {
    return await hash(password, 12);
  }

  comparePasswords(
    password: string,
    storedPasswordHash: string,
  ): Promise<string> {
    return compare(password, storedPasswordHash);
  }

  // async createJwtToken
}
