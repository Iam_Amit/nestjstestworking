import {
  Controller,
  Post,
  Body,
  Res,
  HttpStatus,
  Get,
  Put,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { UserInfoService } from './userinfo.service';
import { Response } from 'express';
import { UserSignupDto } from '../../dto/userSignup.dto';
import { CreateUserDto } from 'src/dto/createUser.dto';
import { UpdateUserDto } from 'src/dto/updateUser.dto';
import { UserLoginDto } from 'src/dto/userLogin.dto';
// import { LocalAuthGuard } from 'src/auth/local.auth.guard';

@Controller('userInfo')
export class UserInfoController {
  constructor(private userInfoService: UserInfoService) {}

  // Add the Data
  // @UseGuards(LocalAuthGuard)
  @Post()
  async addUserInfo(@Body() body: UserSignupDto, @Res() res: Response) {
    const val = await this.userInfoService.createUser(body);
    const { id, email, password, age } = val;
    res.status(HttpStatus.OK).send({
      id,
      email,
      password,
      age,
    });
  }

  // Login User API

  @Post('login')
  async login(@Body() body: UserLoginDto, @Res() res: Response) {
    const loginVal: any = await this.userInfoService.userLogin(body);
    // const { email, password } = loginVal;
    res.status(HttpStatus.OK).send({
      msg: ' User Logged in Successfully',
    });
  }

  // List the Data
  @Get()
  async readUserInfo(@Res() res: Response) {
    const val = await this.userInfoService.getUserInfo();
    res.status(HttpStatus.OK).send(val);
  }

  // Listing the data by ID:

  @Get(':id')
  async readUserInfoId(@Param() id: number, @Res() res: Response) {
    const val = await this.userInfoService.getUserInfoById(id);
    res.status(HttpStatus.OK).send(val);
  }

  // Update the data
  @Put(':id')
  async updateUserInfo(
    @Body() body: UpdateUserDto,
    @Param('id') id: number,
    @Res() res: Response,
  ) {
    const val = await this.userInfoService.updateUserInfo(body, id);
    res.status(HttpStatus.OK).send(val);
  }

  // Hard remove the DATA FROM THE DB
  // @Delete(':id')
  // async removeUserInfo(
  //   @Param('id') id: number,
  //   @Res() res: Response
  // ) {
  //   const val = await this.userInfoService.removeUserInfo(id);
  //   res.status(HttpStatus.OK).send({msg: 'data is Hard deleted from the DB'});
  //   return val;
  // }

  // soft delete the data from the Db

  @Delete(':id')
  removeUserInfoById(@Param(':id') id: number) {
    return this.userInfoService.removeUserInfoById(id);
    // res.status(HttpStatus.OK).send(val)
    // return val;
  }
}
