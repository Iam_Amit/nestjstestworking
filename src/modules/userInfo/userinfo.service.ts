import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserInfoRepository } from './userInfo.repository';
import { UserSignupDto } from 'src/dto/userSignup.dto';
import { UserInfo } from './userInfo.entity';
import { CreateUserDto } from 'src/dto/createUser.dto';
// import { AuthService } from 'src/auth/services/auth/auth.service';
import { UpdateUserDto } from 'src/dto/updateUser.dto';
import { UserLoginDto } from 'src/dto/userLogin.dto';

@Injectable()
export class UserInfoService {
  constructor(
    @InjectRepository(UserInfoRepository)
    private userInfoRepo: UserInfoRepository,
  ) {}
  // Adding the data
  createUser(userInfo: UserSignupDto): Promise<UserInfo> {
    return this.userInfoRepo.addUserInfo(userInfo);
  }

  // Login User Data
    async userLogin(data: UserLoginDto) {
    const emailExist = await this.userInfoRepo.findOne({ email: data.email });

    if (!emailExist) throw new Error('Email not exist ');

    console.log('qwertrwqer =>', emailExist);
    const verifyPasswords = await this.userInfoRepo.comparePasswords(
      data.password,
      emailExist.password,
    );
    
    console.log('VerifyPassword = >', verifyPasswords)
    if (!verifyPasswords) throw new Error(' Password do not match !');

    return 'User loged in Successfuly!';
  }

  //Listing the data
  async getUserInfo() {
    const value = await this.userInfoRepo.find();
    return value;
  }
  // List the data by Id

  async getUserInfoById(id: number) {
    const val = await this.userInfoRepo.findOne(id);
    return val;
  }

  //Updating the data by Id
  async updateUserInfo(userInfo: UpdateUserDto, id: number) {
    // const val = await this.userInfoRepo.findOne(id)
    const val = await this.userInfoRepo.findOne(id);
    // val.email = userInfo.email,
    // val.password = userInfo.password,
    (val.age = userInfo.age),
      (val.isActive = userInfo.isActive),
      // (val.username = userInfo.username),
      val.save();
    return val.save();
  }

  // Remove the user from the db
  // async removeUserInfo(id: number) {
  //     const val = await this.userInfoRepo.delete(id)
  //         return val;
  // }

  // Remove the user from the db SOFT DELETE

  async removeUserInfoById(id: number) {
    const val = await this.userInfoRepo.findOne(id);
    val.isActive = false;
    val.save();
    return 'User soft delete';
  }
}
