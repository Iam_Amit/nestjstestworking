import { Repository, EntityRepository } from 'typeorm';
import { UserInfo } from './userInfo.entity';
import { UserSignupDto } from 'src/dto/userSignup.dto';
// import { from, Observable } from 'rxjs';
import { hash, compare } from 'bcrypt';
import { InternalServerErrorException } from '@nestjs/common';

@EntityRepository(UserInfo)
export class UserInfoRepository extends Repository<UserInfo> {
  async addUserInfo(userInfo: UserSignupDto): Promise<UserInfo> {
    const { email, password, username } = userInfo;
    const newUser = new UserInfo();
    newUser.username = username;

    try {
      newUser.password = await this.hashPassword(password);
    } catch (e) {
      throw new InternalServerErrorException({ error: e });
    }
    // newUser.age = age;
    newUser.email = email;
    newUser.isActive = true;
    await newUser.save();
    return newUser;
  }
  async hashPassword(password: string): Promise<string> {
    return await hash(password, 12);
  }

  comparePasswords(
    password: string,
    storedPasswordHash: string,
  ): Promise<string> {
    return compare(password, storedPasswordHash);
  }
}
