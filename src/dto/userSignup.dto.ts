// import { isNotEmpty } from "class-validator";

import {
  IsEmail,
  IsNotEmpty,
  MaxLength,
  IsString,
  MinLength,
  IsAlphanumeric,
} from 'class-validator';

export class UserSignupDto {
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @IsNotEmpty()
  @IsAlphanumeric()
  @MaxLength(20)
  @MinLength(8)
  password: string;

  @IsAlphanumeric()
  username: string;
}
