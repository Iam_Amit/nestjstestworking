//UserLoginDto
import {
  IsEmail,
  IsNotEmpty,
  MaxLength,
  IsString,
  MinLength,
  IsAlphanumeric,
} from 'class-validator';

export class UserLoginDto {
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @IsNotEmpty()
  // @IsAlphanumeric()
  @MaxLength(20)
  @MinLength(6)
  password: string;

  // @IsAlphanumeric()
  // username: string;
}
