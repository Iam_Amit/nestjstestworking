import { IsAlphanumeric, IsString } from 'class-validator';

export class UpdateUserDto {
  @IsString()
  age: number;

  isActive: boolean;

  @IsAlphanumeric()
  username: string;
}
