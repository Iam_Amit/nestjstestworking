import { IsString, IsAlphanumeric } from 'class-validator';

export class CreateUserDto {
  @IsString()
  age: number;

  isActive: boolean;
  id: number;

  @IsAlphanumeric()
  username: string;
}
