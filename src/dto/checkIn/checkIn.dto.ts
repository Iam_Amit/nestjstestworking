// import { PrimaryGeneratedColumn } from 'typeorm';

import { PrimaryColumn } from "typeorm";

export class CheckInDto {
  // @PrimaryGeneratedColumn()
  // @PrimaryColumn()
  userId: number;

  checkIn: Date;
}
