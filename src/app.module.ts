import { TimePunchService } from './modules/timePunch/timepunch.service';
import { TimePunchController } from './modules/timePunch/timepunch.controller';
// import { TimePunchController } from './modules/timePunch/timepunch.controller';
// import { TimePunchService } from './modules/timePunch/timepunch.service';
// import { CheckInController } from './modules/checkin/checkin.controller';
// import { CheckInService } from './modules/checkin/checkin.service';
import { CheckInModule } from './modules/checkin/checkin.module';
import { UserAuthModule } from './modules/userAuth/userauth.module';
import { UserInfoModule } from './modules/userInfo/userinfo.module';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { join } from 'path';
import { TimePunchModule } from './modules/timePunch/timepunch.module';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
    TimePunchModule,
    CheckInModule,
    UserAuthModule,
    UserInfoModule,
    TypeOrmModule.forRoot({
      type: 'postgres',
      port: 5432,
      host: 'localhost',
      username: 'postgres',
      password: 'root',
      logging: 'all',
      logger: 'advanced-console',
      database: 'user',
      entities: [join(__dirname, './**/**.entity{.ts,.js}')],
      migrationsTableName: 'Migrations_History',
      synchronize: true,
    }),
    AuthModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
