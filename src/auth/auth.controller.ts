import { Body, Controller, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthCredentialsDto } from './dto/auth.credentials.dto';

@Controller('auth')
export class AuthController {

    constructor(
        private authService: AuthService
    ) {}


    @Post('/signup')
    signUp(@Body() authCredentialDto: AuthCredentialsDto): Promise<void> {

        // console.log('Auth Credential DTO ', authCredentialDto)

        return this.authService.signUp(authCredentialDto);
    }
     
}
